<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Berichten extends Model
{
	protected $table = "berichten";

	protected function scopeGetBerichten(){
		$query = DB::table('berichten')
			->select('id','title', 'content')
			->get()
			->toArray();
		$contents = json_decode(json_encode($query), true);
		return $contents;
	}

	protected function scopeInsertBericht($request){
		$title = $request->input('title');
		$content = $request->input('content');
		DB::table('berichten')->insert(['title' => $title, 'content' => $content]);
		return Berichten::scopeGetBerichten();
	}

	protected function scopeDeleteBericht($request){
		$id = $request->input('id');
		DB::table('berichten')
		->where('id', $id)
		->delete();
		return Berichten::scopeGetBerichten();
	}

	protected function scopeUpdateBericht($request){
		$title = $request->input('title');
		$content = $request->input('content');
		$id = $request->input('id');

		DB::table('berichten')
		->where ('id', $id)
		->update(['title' => $title, 'content' => $content]);
		return Berichten::scopeGetBerichten();
	}

	/**
	 * Get the relationships for the entity.
	 *
	 * @return array
	 *
	 */
	public function getQueueableRelations()
	{
		// TODO: find out why in gods name I need to implement this may have to something to do with the fact that i'm working on a old laravel version??
	}
}
