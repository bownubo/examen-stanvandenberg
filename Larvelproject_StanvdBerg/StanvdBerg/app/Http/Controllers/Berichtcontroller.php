<?php

namespace App\Http\Controllers;

use App\Berichten;
use Illuminate\Http\Request;


class Berichtcontroller extends Controller
{
	public function index()
	{
		$results = Berichten::scopeGetBerichten();
		return view('home', ['berichten' => $results]);
	}
	public function addbericht(Request $request){
		$results = Berichten::scopeInsertBericht($request);
		return view('home', ['berichten' => $results]);

	}

	public function deletebericht(Request $request){
		$results = Berichten::scopeDeleteBericht($request);
		return view('home', ['berichten' => $results]);
	}

	public function updatebericht(Request $request){
		$results = Berichten::scopeUpdateBericht($request);
		return view('home', ['berichten' => $results]);
	}
}
