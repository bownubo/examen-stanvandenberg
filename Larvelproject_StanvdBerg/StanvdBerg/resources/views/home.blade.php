@extends('layouts.app')

@section('content')

<style>
    table {
        border-collapse:collapse;
        margin-top: 100px;
        padding: 5px;
    }

    textarea {
        width:100%;
    }

    .titel {
        text-align: center;
    }

</style>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-1">
            <div class="panel panel-default">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="panel-heading">Welkom bij het examen CMS</div>
                <div class="panel-body">
                    <form action="/home" method="POST">

                        {{ csrf_field() }}

                        Bericht titel
                        <br>
                        <input type="text" name="title">
                        <br>
                        Bericht content<br>
                        <textarea name="content"></textarea><br>
                        <input type="submit" name="submit" value="Voeg bericht toe">
                    </form>

                    @foreach($berichten as $row)
                    <table class="col-md-8 col-md-offset-2">
                        <form method='POST' action="/home/update">
                            {{ csrf_field() }}
                            <tbody>
                            <tr>
                                <td class="titel">
                                    <label>bericht titel</label><br/>
                                    <input type='text' name='title' value='{{$row["title"]}}'/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>bericht content</label><br>
                                    <textarea rows='5' cols='60' name='content'>{{$row["content"]}}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="hidden" value="{{$row["id"]}}" name="id" />
                                    <input type='submit' name='update' value='update bericht'>

                        </form>
                        <form method='POST' action="/home/delete">
                            {{ csrf_field() }}
                            <input type="hidden" value="{{$row["id"]}}" name="id" />
                            <input type='submit' name='delete' value='Verwijder bericht' />
                        </form>
                        </td>
                        </tr>
                        </tbody>
                    </table>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
