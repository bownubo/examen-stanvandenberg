<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::get('/home', 'Berichtcontroller@index')->name('home');
Route::post('/home', 'Berichtcontroller@addbericht');
route::post('/home/delete', 'Berichtcontroller@deletebericht');
route::post('/home/update', 'Berichtcontroller@updatebericht');

Auth::routes();


